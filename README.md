# Docker

- Créer et exécuter une image en tant que conteneur
- Partager des images à l'aide de Docker Hub
- Déployer des application Docker à l'aide de plusieurs conteneurs avec une base de données
- Exécuter des applications à l'aide de Docker Compose

## Installation de Docker Desktop pour Windows

[Docker Desktop (.exe)](https://desktop.docker.com/win/stable/amd64/Docker%20Desktop%20Installer.exe)

## **Partie 1 : Lancement du tutoriel**

Pour commencer le dictaciel, exécuter la commande :
```
docker run -d -p 80:80 docker/getting-started
```
- ```-d``` : exécuter le conteneur en mode détaché (en arrière-plan)
- ```-p 80:80``` : mapper le port 80 de l'hôte sur le port 80 du conteneur
- ```docker/getting-started``` : l'image à utiliser

Il est possible de combiner des drapeaux en un seul caractère pour raccourcir la commande complète :
```
docker run -dp 80:80 docker/getting-started
```
### **Dashboard Docker**

Le dashboard Docker permet de donner un aperçu rapide des conteneurs exécutés sur la machine.

### **Qu'est ce qu'un conteneur**

Un conteneur est un autre processus sur la machine qui à été isolé de tous les autres processus sur la machine hôte. 

**Points forts de Docker** :
- **Facilité de chargement d’un processus dans un conteneur** : un conteneur vide peut être
récupéré et instancié en une ligne de commande (le logiciel Docker et un ordinateur connecté à
Internet sont les seuls prérequis), et le lancement d’un processus dans le conteneur se fait
exactement de la même manière que sur une machine locale.
- **Ce chargement peut être automatisé** par la réalisation d’un fichier contenant un texte écrit dans un format décrivant les étapes de chargement du processus dans le conteneur.
-  **Manutention du conteneur** : toute personne ayant créé un conteneur par remplissage avec un
processus configuré comme elle le souhaite peut ensuite diffuser le tout par Internet, en ayant
la garantie que n’importe quelle autre personne pourra exécuter le conteneur.
- **Rapidité d’exécution** : le conteneur regroupant tout le paramétrage, les livrables, les ressources
et tout ce qui est utile au programme supporté, son lancement se fait quasi immédiatement, et
sans nécessiter de temps d’installation.
- **Interchangeabilité** : toute personne exécutant le conteneur obtiendra exactement le même
résultat que l’émetteur. Cette approche standard permet de garantir une même exécution des
programmes, et ce quel que soit le système sous-jacent, pourvu qu’il soit pourvu du programme
Docker.
- **Indépendance par rapport à l’infrastructure sous-jacente qui porte les conteneurs** : ces derniers
peuvent être exécutés indifféremment sur un serveur Docker local à une machine, sur un
cluster de serveurs Docker installés sur des machines en réseau (on parle du mode Swarm de
Docker, sur lequel nous reviendrons plus loin), sur un cloud proposant la fonctionnalité de
Container as a Service, etc.

### **Qu'est-ce qu'une image de conteneur**

Lorsqu'un conteneur fonctionne, il utilise un système de fichiers isolé. Ce système de fichiers personnalisé est fourni par une image de conteneur. Puisque l'image contient le système de fichiers du conteneur, elle doit contenir tout ce qui est nécessaire pour exécuter une application - toutes les dépendances, la configuration, les scripts, les binaires, etc. L'image contient également d'autres éléments de configuration du conteneur, tels que des variables d'environnement, une commande par défaut à exécuter et d'autres métadonnées.

## **Partie 2 : Exemple d'application**

[Télécharger le contenu de l'application](https://github.com/docker/getting-started/tree/master/app)

### **Construire l'image du conteneur de l'application**

Afin de construire l'application, nous devons utiliser un Dockerfile. Un Dockerfile est simplement un script d'instructions en texte qui est utilisé pour créer une image de conteneur.

Créez un fichier nommé Dockerfile dans le même dossier que le fichier package.json avec le contenu suivant:
```Dockerfile
# syntax=docker/dockerfile:1
FROM node:12-alpine
RUN apk add --no-cache python g++ make
WORKDIR /app
COPY . .
RUN yarn install --production
CMD ["node", "src/index.js"]
```
**Attention**, vérifier que le fichier ```Dockerfile``` n'a pas d'extension de fichier comme ```.txt```.

Ouvrir un terminal et aller dans le répertoire ```app``` puis créer l'image du conteneur à l'aide de la commande ```docker build```:
```
docker build -t getting-started .
```
Cette commande a utilisé le Dockerfile pour construire une nouvelle image de conteneur. Un grand nombre de "couches" ont été téléchargées. Ceci est dû au fait que nous avons indiqué au constructeur que nous voulions commencer à partir de l'image ```node:12-alpine```. Mais, comme nous ne l'avions pas sur notre machine, cette image a dû être téléchargée.

Après le téléchargement de l'image, nous avons copié notre application et utilisé ```yarn``` pour installer les dépendances de notre application. La directive ```CMD``` spécifie la commande par défaut à exécuter lors du démarrage d'un conteneur à partir de cette image.

Enfin, l'indicateur ```-t``` marque notre image. Il s'agit simplement d'un nom lisible par l'homme pour l'image finale. Puisque nous avons nommé l'image ```getting-started```, nous pouvons faire référence à cette image lorsque nous lançons un conteneur.

Le ```.``` à la fin de la commande ```docker build``` indique que Docker doit rechercher le Dockerfile dans le répertoire actuel.

### **Démarrer un conteneur d'applications**

Maintenant que l'image est créée, exécuter l'application avec la commande ```docker run``` et spécifier le nom de l'image:
```
docker run -dp 3000:3000 getting-started
```
Ouvrir un navigateur web : http://localhost:3000/

![Application](/img/capture-app.PNG)

Essayer d'ajouter un ou deux éléments pour tester que l'application fonctionne comme prévue.

Revenir sur l'application Docker, il doit y avoir deux conteneurs en cours d'exécution.
![Conteneurs en cours d'exécution](/img/capture-docker-execution.PNG)

## **Partie 3 : Mettre à jour l'application**

### **Mettre à jour le code source**

Dans le fichier ```src/static/js/app.js```, mettre à jour la ligne 56 :
```
-       <p className="text-center">No items yet! Add one above!</p>
+       <p className="text-center">You have no todo items yet! Add one above!</p>
```
Construire l'image mise à jour :
```
docker build -t getting-started .
```
Démarrrer un nouveau conteneur en utilisant le code mis à jour :
```
docker run -dp 3000:3000 getting-started
```
**Erreur**
```
docker: Error response from daemon: driver failed programming external connectivity on endpoint mystifying_bell (620dd63aca867600f8a23ac0b5e248542c853566532c391dcc0bfcca1780e764): Bind for 0.0.0.0:3000 failed: port is already allocated.
```
Il n'est pas possible de démarrer le nouveau conteneur car l'ancien conteneur est toujours en cours d'exécution. La raison est que ce conteneur utilise le port 3000 de l'hôte et qu'un seul processus sur la machine (conteneurs inclus) peut écouter un port spécifique. Pour résoudre, il faut suprimer l'ancien conteneur.

### **Remplacer l'ancien conteneur**

Pour retirer un conteneur, il doit d'abord être arrêté. Il existe deux façons de pouvoir retirer l'ancien conteneur.

**Supprimer un conteneur à l'aide de la CLI**

1. Obtenir l'ID du conteneur à l'aide de la commande ```docker ps```.
```
docker ps
```

2. Utilise la commande ```docker stop``` pour arrêter la conteneur.
```
docker stop <the-container-id>
```

3. Une fois le conteneur arrêté, le supprimer à l'aide de la commande ```docker rm```.
```
docker rm <the-container-id>
```

> Il est possible d'arrêter et de supprimer un conteneur en une seule commande en ajoutant l'indicateur "force" à la commande ```docker rm```. Par exemple: ```docker rm -f <the-container-id>```

**Supprimer un conteneur à l'aide du dashboard Docker**

1. Avec le tableau de bord ouvert, survolez le conteneur de l'application et vous verrez une collection de boutons d'action apparaître sur la droite.

2. Cliquez sur l'icône de la corbeille pour supprimer le conteneur.

3. Confirmez la suppression et le tour est joué !

### **Démarrer le conteneur d'applications mis à jour**
```
docker run -dp 3000:3000 getting-started
```
Actualiser le navigateur, le texte doit être mis à jour:

![Application mise à jour](/img/capture-app-maj.PNG)

## **Partie 4 : Partager l'application**
Pour partager des images Docker, vous devez utiliser un registre Docker. Le registre par défaut est Docker Hub.
> **Identifiant Docker**
> 
> Un Docker ID permet d'accéder à Docker Hub, la plus grande bibliiothèque et communauté au monde pour les images de conteneurs.

### **Créer un dépôt**
1. Inscription 
2. Connexion à Docker Hub
3. Cliquer sur le bouton  **Créer un référentiel**
4. Pour le nom du référentiel, utiliser ```getting-started```. S'assurer que la visibilité est ```Public```.
5. Cliquer sur le bouton **Créer**

### **Pousser l'image**

1. Essayer d'exécuter la commande push disponible sur Docker Hub.
```
docker push docker/getting-started
```
> La commande utilise l'espace de nom, pas "docker"

La commande push recherchait une image nommée docker/getting-started, mais n'en a pas trouvé. Si vous courez ```docker image ls```, vous n'en verrez pas non plus. Pour résoudre ce problème, il faut "taguer" l'image existante pour lui donner un autre nom.

2. Se connecter au Docker Hub avec la commande : ```docker login -u YOUR-USER-NAME```
3. Utiliser la commande ```docker tag``` pour donner un nouveau nom à l'image ```getting-started```.
```
docker tag getting-started melisandebaratte/getting-started
```
4. Essayer à nouveau la commande push :
```
docker push melisandebaratte/getting-started
```

### **Exécutez l'image sur une nouvelle instance**

Maintenant que l'immage est créée et insérée dans un registre, essayer d'exécuter l'application sur une nouvelle intance qui n'a jamais vu cette image de conteneur. 

1. Ouvrir le navigateur vers [Play with Docker](https://labs.play-with-docker.com/)
2. Cliquer sur connexion puis sélectionner docker dans la liste déroulante
3. Se connecter avec son compte Docker Hub
4. Une fois connecté, cliquer sur l'option "Add new instance" dans la barre latérale gauche.

![Play with Docker](/img/capture-play-docker.PNG)

5. Dans le terminal, démarrer l'application fraîchement poussée.
```
docker run -dp 3000:3000 YOUR-USER-NAME/getting-started
```
6. Cliquer sur le badge 3000 lorsqu'il apparaît. L'application doit apparaître avec les modifications. Si le badget 300 ne s'affiche pas, cliquer sur le bouton "ouvrir le port" et saisir 3000.

## **Partie 5 : persistance de la base de données**

La liste de tâche est effacée à chaque fois que je relance le conteneur ...

### **Le système de fichiers du conteneur**
Lorsqu'un conteneur s'exécute, il utilise les différentes couches d'une image pour son système de fichiers. Chaque conteneur dispose également de son propre "espace scratch" pour créer/mettre à jour/supprimer des fichiers. Tout changement ne sera pas vu par un autre conteneur, même s'il utilise la même image.

### **Voir dans la pratique**
Pour voir cela, il faut démarrer deux conteneurs et créer un fichier dans chacun. Les fichiers créés dans un conteneur nes ont pas disponibles dans un autre.

1. Démarrer un conteneur ```ubuntu``` qui créera un fichier nommé ```/data.txt``` avec un nombre aléatoire compris entre 1 et 10000.
```
docker run -d ubuntu bash -c "shuf -i 1-10000 -n 1 -o /data.txt && tail -f /dev/null"
```

2. Ouvrir le dashboard et cliquer sur la première action du conteneur qui exécute l'image ```ubuntu```:

![dashboard Docker](/img/capture-conteneur-cli.PNG)

Exécuter la commande suivante pour voir le contenu du fichier /data.txt. Fermer ensuite la terminal.
```
cat /data.txt
```
Possible d'utiliser la commande ```docker exec```. Il faut d'abord obtenir l'ID du conteneur (```docker ps```):
```
docker exec <container-id> cat /data.txt
```
On obtient un nombre aléatoire.

3. Démarrer un autre conteneur ```ubuntu``` (même image) et on verra que l'on n'a pas le fichier.
```
docker run -it ubuntu ls /
```
Il n'y a pass de fichier ```data.txt``` parce qu'il a été écrit dans l'espace de travail pour le premier conteneur uniquement.

4. Supprimer le premier conteneur à l'aide de la commande ```docker rm -f <container-id>```

### **Volume de conteneur**

Les conteneurs peuvent créer, mettre à jour et supprimer des fichiers, ces modifications sont perdues lorque le conteneur est supprimé et toutes les modifications sont isolées d,as ce conteneur. Avec les volumes, on peut changer ça.

Les volumes offrent la possibilité de reconnecter des chemins de fichiers systèmes spécifiques du conteneur à la machine hôte. 
Les modifications apportées à un répertoire sont également visibles sur la machine hôte. Idem, lors des redémmarages du conteneur, il y aura les mêmes fichiers.

### **Persister les données de l'application**

Par défaut, l'application todo stocke ses données dans une base de données SQLite à ```/etc/todos/todo.db```. 

En créant un volume et en l'attachant au répertoire dans lequel les données sont stockées, elles seront conservées. Au fur et à mesure que le conteneur écrit dans le fichier todo.db, il sera conservé sur l'hôte dans le volume.

1. Créer un volume nommé avec la commande ```docker volume create```. 

Considérez un volume nommé comme un simple seau de données. Docker conserve l'emplacement physique sur le disque et il suffit de se souvenir du nom du volume. Chaque fois qu'on utilise le volume, Docker s'assurera que les données fournies sont correctes.

```
docker volume create todo-db
```

2. Arrêtez et supprimer à nouveau le conteneur d'application dans le dashboard ou en ligne de commande car il est toujours en cours d'exécution sans utiliser le volume persistant.

3. Démarrer le conteneur d'application, ajouer le flag ```-v``` pour spécifier un montage de volume. On utilisera le volume nommé et /etc/todos captura tous les fichiers créés en chemin.
```
docker run -dp 3000:3000 -v todo-db:/etc/todos getting-started
```

4. Une fois le conteneur démarré, ouvrir l'application et ajouter quelques éléments à la liste.

![Persistance de la base de données](/img/capture-app-persist.PNG)

5. Arrêter et supprimer le conteneur. Utiliser le dashboard ou la commande ```docker ps``` pour obtenir l'ID puis ```docker rm -f <id>```

6. Démarrer un nouveau conteneur avec la même commande ci-dessus.

7. Ouvrir l'application, les éléments de la liste doivent toujours être là.

8. Retirer le conteneur.


> **Remarque**
> 
> Alors que les volumes nommés et les montages liés (dont nous parlerons dans une minute) sont les deux principaux types de volumes pris en charge par une installation par défaut du moteur Docker, il existe de nombreux plugins de pilote de volume disponibles pour prendre en charge NFS, SFTP, NetApp, etc. Cela sera particulièrement important lorsque vous commencerez à exécuter des conteneurs sur plusieurs hôtes dans un environnement en cluster avec Swarm, Kubernetes, etc.


### **Dans un volume**

Où Docker stocke-t-il réellement les données lorsqu'on utilise un volume nommé ?

Il est possible de le savoir avec la commande ```docker volume inspect```.

```
[
    {
        "CreatedAt": "2021-08-23T14:05:03Z",
        "Driver": "local",
        "Labels": {},
        "Mountpoint": "/var/lib/docker/volumes/todo-db/_data",
        "Name": "todo-db",
        "Options": {},
        "Scope": "local"
    }
]
```
Le ```Mountpoint``` est l'emplacement réel sur le disque où les données sont stockées. Sur la plupart des machines, vous aurez besoin d'un accès root pour accéder à ce répertoire depuis l'hôte.

## **Partie 6: Utiliser les bind mounts**

Précedemment, on a utilisé un volume nommé pour conserver les données dans la base de données. Les volumes nommés sont parfaits si on veut simplement stocker des données car on n'a pas à se soucier de l'endroit où les données sont stockées.

Avec bind mounts, on contrôle le mountpoint exact sur l'hôte. On peut l'utiliser pour conserver des données mais il est souvent utilisé pour fournir des données supplémentaires dans des conteneurs.


### **Comparaison rapides des types de volumes**

Les bind mounts et les volumes nommés sont les deux principaux types de volumes fournis avec le moteur Docker. Cependant, des pilotes de volume supplémentaires sont disponibles pour prendre en charge d'autres cas d'utilisation ( SFTP , Ceph , NetApp , S3 , etc.).


|            | Volumes nommés | Bind mounts |
|:----------:|:--------------:|:-----------:|
|Emplacement de l'hôte | Docker choisit | On contrôle |
|Exemple de montage (en utilisant -v) | mon-volume:/usr/local/data | /chemin/vers/données:/usr/local/data |
|Remplit le nouveau volume avec le contenu du conteneur | Oui | Non |
|Prend en charge les pilotes de volume | Oui | Non |

### **Démarrer un conteneur en mode dev**

Pour exécuter notre conteneur afin de prendre en charge un workflow de développement, nous procéderons comme suit :
- Monter le code source dans le conteneur
- Installez toutes les dépendances, y compris les dépendances « dev »
- Démarrez nodemon pour surveiller les modifications du système de fichiers

1. S'assurer qu'aucun conteneur ```getting-started``` n'est en cours d'exécution/
2. Exécuter la commande suivante:
```
 docker run -dp 3000:3000 `
     -w /app -v "$(pwd):/app" `
     node:12-alpine `
     sh -c "yarn install && yarn run dev"
```
- ```-dp 3000:3000```: pareil qu'avant. Exécuter en mode détaché (arrière-plan) et créer un mappage de port
- ```-w /app```: définit le "répertoire de travail" ou le répertoire courant à partir duquel la commande s'exécutera
- ```-v "$(pwd):/app"```: bind monte le répertoire courant de l'hôte dans le conteneur dans le répertoire /app 
- ```node:12-alpine```: l'image à utiliser. Il s'agit de l'image de base de notre application à partir du Dockerfile
- ```sh -c "yarn install && yarn run dev"```: On démarre un shell en utilisant ```sh```(alpine n'a pas ```bash```) et en l'exécutant ```yarn install``` pour installer toutes les dépendances, puis en exécutant ```yarn run dev```. Si on regarde dans le ```package.json```, nous verrons que le script ```dev``` démarre ```nodemon```.

3. On peut regarder les logs en en utilisant la commande ```docker logs -f <container-id>```
```
nodemon src/index.js
[nodemon] 1.19.2
[nodemon] to restart at any time, enter `rs`
[nodemon] watching dir(s): *.*
[nodemon] starting `node src/index.js`
Using sqlite database at /etc/todos/todo.db
Listening on port 3000
```
Pour quitter, appuyer sur ```ctrl```+```c```.

4. Modifier l'application. Dans le fichier ```src/static/js/app.js```, modifier le bouton "Add Item" en "Add" à la ligne 109:
```
 -      {submitting ? 'Adding...' : 'Add Item'}
 +      {submitting ? 'Adding...' : 'Add'}
```

5. Actualiser la page.

![Modification de l'app](/img/capture-app-maj-2.PNG)

6. Apporter autant de modification que l'on souhaite ... Lorsque c'est terminé, arrêter le conteneur et créer une nouvelle image avec ```docker build -t getting-started .``` .

L'utilisation de bind mounts est très courante pour les configurations de développement local. L'avantage est que la machine de développement n'a pas besoin d'avoir tous les outils et environnements de construction installés. Avec une seule commande ```docker run```, l'environnement de développement est extrait et prêt à fonctionner. 

## **Partie 7: Applications multi-conteneur**

### **Mise en réseau des containers**
Ne pas oublier que les conteneurs, par défaut, s'exécutent de manière isolée et ne connaissent rien des autres processus ou conteneurs sur la même machine.
Alors, comment permettons-nous à un conteneur de parler à un autre ? La réponse est le réseautage.

> Note
> 
> Si deux conteneurs sont sur le même réseau, ils peuvent se parler. S'ils ne le sont pas, ils ne peuvent pas.


### **Démarrer MySQL**
Il existe deux manières de mettre un conteneur sur un réseau:
- 1. L'attribuer au démarrage
- 2. Connecter un conteneur existant.

1. Créer le réseau:
```
docker network create todo-app
```
2. Démarrer un conteneur MySQL et le connecter au réseau. On va définir quelques variables d'environnement que la base de données utilisera pour initialiser.
```PowerShell
docker run -d `
     --network todo-app --network-alias mysql `
     -v todo-mysql-data:/var/lib/mysql `
     -e MYSQL_ROOT_PASSWORD=secret `
     -e MYSQL_DATABASE=todos `
     mysql:5.7
```

> On utilise un volume nommé ```todo-mysql-data``` et qu'on monte sur ```/var/lib/mysql```, c'est là que MySQL stocke ses données. Cependant, on n'a jamais exécuté de commande ```docker volume create```. Docker reconnaît que l'on veut utiliser un volume nommé et en crée un automatiquement.

3. Pour vérifier que la base de données est opérationnelle, se connecter à la base de données et vérifier qu'elle se connecte.
```
docker exec -it <mysql-container-id> mysql -u root -p
```
Lorsque l'invite de mot de passe s'affiche, saisir ```secret```. Dans le shell MySQL, répertorier les bases de données et vérifier que la base de données ```todos``` soit présente.
```
SHOW DATABASES;
```
Résultat:
```
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| sys                |
| todos              |
+--------------------+
5 rows in set (0.01 sec)
```

La base de données ```todos``` est prête à être utilisée.

### **Se connecter à MySQL**

Si on exécute un autre conteneur sur le même réseau, comment trouve t-on le conteneur (rappel: chaque conteneur a sa propre adresse IP) ?

Pour le comprendre, on va utiliser le conteneur [nicolaka/netshoot](https://github.com/nicolaka/netshoot) , qui est livré avec de nombreux outils utiles pour le dépannage ou le débogage des problèmes de réseau.

1. Démarrez un nouveau conteneur à l'aide de l'image nicolaka/netshoot. Assurez-vous de le connecter au même réseau.
```
docker run -it --network todo-app nicolaka/netshoot
```

2. A l'intérieur du conteneur, on utlise la commande ```dig```, qui un outil DNS. Et on recherche l'adresse IP de l'hôte ```mysql```.
```
dig mysql
```
Réponse:
```
; <<>> DiG 9.16.19 <<>> mysql
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 47776
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 0

;; QUESTION SECTION:
;mysql.                         IN      A

;; ANSWER SECTION:
mysql.                  600     IN      A       172.19.0.2

;; Query time: 10 msec
;; SERVER: 127.0.0.11#53(127.0.0.11)
;; WHEN: Tue Aug 24 10:56:04 UTC 2021
;; MSG SIZE  rcvd: 44
```

Dans la section "ANSWER", l'enregistrement ```A``` pour ```mysql``` qui se résout à ```172.19.0.2```. Alors que mysql n'est normalement pas un nom d'hôte valide, Docker a pu le résoudre à l'adresse IP du conteneur qui avait cet alias de réseau (```--network-alias``` utilisé plus tôt).

### **Exécuter l'application avec MySQL**

L'application todo prend en charge la configuration de quelques variables d'environnement pour spécifier les paramètres de connexion MySQL. Elles sont:

- ```MYSQL_HOST```: le nom d'hôte du serveur MySQL en cours d'exécution
- ```MYSQL_USER```: le nom d'utilisateur à utiliser pour la connexion
- ```MYSQL_PASSWORD```: le mot de passe à utiliser pour la connexion
- ```MYSQL_DB```: la base de données à utiliser une fois connecté

> Définition des paramètres de connexion via Env Vars
>
> Bien que l'utilisation de vars env pour définir les paramètres de connexion soit généralement acceptable pour le développement, elle est FORTEMENT DÉCONSEILLÉE lors de l'exécution d'applications en production.
>
> Un mécanisme plus sécurisé consiste à utiliser la prise en charge des secrets fournis par votre infrastructure d'orchestration de conteneurs. Dans la plupart des cas, ces secrets sont montés sous forme de fichiers dans le conteneur en cours d'exécution. Vous verrez que de nombreuses applications (y compris l'image MySQL et l'application todo) prennent également en charge les variables env avec un suffixe _FILE pour pointer vers un fichier contenant la variable.
> 
> Par exemple, la définition de la var MYSQL_PASSWORD_FILE obligera l'application à utiliser le contenu du fichier référencé comme mot de passe de connexion. Docker ne fait rien pour prendre en charge ces variables d'environnement. Votre application devra savoir rechercher la variable et obtenir le contenu du fichier.

1. Spécifier chacune des variables d'environnement ainsi que la connexion du conteneur au réseau d'applications.
``` PowerShell
docker run -dp 3000:3000 `
   -w /app -v "$(pwd):/app" `
   --network todo-app `
   -e MYSQL_HOST=mysql `
   -e MYSQL_USER=root `
   -e MYSQL_PASSWORD=secret `
   -e MYSQL_DB=todos `
   node:12-alpine `
   sh -c "yarn install && yarn run dev"
```

2. Regarder les logs du conteneur (```docker logs <container-id>```), il doit y avoir un message indiquant qu'il utilise la base de données mysql.
```
nodemon src/index.js
[nodemon] 1.19.2
[nodemon] to restart at any time, enter `rs`
[nodemon] watching dir(s): *.*
[nodemon] starting `node src/index.js`
Waiting for mysql:3306..................................................................................................................
Connected!
Connected to mysql db at host mysql
Listening on port 3000
```

3. Ouvrir l'application dans un navigateur et ajouter quelques éléments à la liste de tâches.

4. Se connecter à la base de données mysql et prouver que les éléments sont inscrits dans la base de données.
```
docker exec -it <mysql-container-id> mysql -p todos
```
Résultat:
```
mysql> select * from todo_items;
+--------------------------------------+------------------+-----------+
| id                                   | name             | completed |
+--------------------------------------+------------------+-----------+
| 96c91dcb-c240-43f1-859a-ce23460f3d32 | cheval           |         0 |
| 00d64933-78ff-46cf-a9d8-b78ee9923e0f | laver la voiture |         0 |
+--------------------------------------+------------------+-----------+
2 rows in set (0.00 sec)
```

Dans le dashboard Docker, on peut voir qu'il y a deux conteneurs d'applications en cours d'exécution. Mais il n'y a aucune indication qu'ils sont régroupés dans une seule application.

## **Partie 8: Utiliser Docker Compose**

Docker Compose est un outil qui a été développé pour aider à définir et partager des applications multi-conteneurs. Avec Compose, il est possible de créer un fichier YAML pour définir les services et avec une seule commande, tout faire tourner ou tout démonter.

Le gros avantage d'utiliser Compose est qu'il est possible de définir sa pile d'applications dans un fichier, le conserver à la racine de du référentiel de projet (il est désormais contrôlé par version) et permettre facilement à quelqu'un d'autre de contribuer au projet.

### **Installer Docker Compose**

Docker compose est présent si l'on a fait l'installation de Docker Desktop.

Docker compose est déjà installé sur les instances de Play-with-Docker.

Sur une machine Linux, il faut faire l'installation.

### **Créer le fichier**
1. A la racine du projet d'application, créer un fichier nommé ```docker-compose.yml```.
2. Dans le fichier de composition, on commence par définir la version du schéma. Dans la plupart des cas, il est préférable d'utiliser la dernière version prise en charge. ([référence du fichier Compose](https://docs.docker.com/compose/compose-file/))
```docker
version: "3.7"
```

3. Définir la liste des services (ou conteneurs) que l'on souhaite exécuter dans le cadre de l'application.
```yml
version: "3.7"

services:
```

### **Definir le service de l'application**
Rappel: C'était la commande utilisée pour définir le conteneur d'applications.
```PowerShell
docker run -dp 3000:3000 `
  -w /app -v "$(pwd):/app" `
  --network todo-app `
  -e MYSQL_HOST=mysql `
  -e MYSQL_USER=root `
  -e MYSQL_PASSWORD=secret `
  -e MYSQL_DB=todos `
  node:12-alpine `
  sh -c "yarn install && yarn run dev"
```

1. Définir l'entrée de service et l'image du conteneur. On peut choisir n'importe quel nom pour le service. Le nom deviendra automatiquement un alias réseau, ce qui sera utilse lors de a définition du service MySQL.
```yml
version: "3.7"

services:
    app:
        image: node:12-alpine
```
2. Ajout de la ```command``` (proche de la définition de l'image)
```yml
version: "3.7"

services:
    app:
        image: node:12-alpine
        command: sh -c "yarn install && yarn run dev"
```
3. Migrer la partie de la commande définissant les ```ports``` pour le service (```-p 3000:3000```).
```yml
version: "3.7"

services:
    app:
        image: node:12-alpine
        command: sh -c "yarn install && yarn run dev"
        ports:
            - 3000:3000
```
4. Migrer, ensuite, le répertoire de travail (```-w /app```) et le mappage de volume (```-v "$pwd:/app"```) en utilisant les définitions ```working_dir```et ```volumes```.

L'un des avantages des définitions de volume Docker Composer est qu'on peut utiliser des chemins relatifs à partif du répertoire actuel.
```yml
version: "3.7"

services:
    app:
        image: node:12-alpine
        command: sh -c "yarn install && yarn run dev"
        ports:
            - 3000:3000
            working_dir: /app
            volumes:
                - ./:/app
```
5. Et enfin, migrer les variables d'environnement:
```yml
version: "3.7"

services:
    app:
        image: node:12-alpine
        command: sh -c "yarn install && yarn run dev"
        ports:
            - 3000:3000
            working_dir: /app
            volumes:
                - ./:/app
            environment:
                MYSQL_HOST: mysql
                MYSQL_USER: root
                MYSQL_PASSWORD: secret
                MYSQL_DB: todos
```

### **Définir le service MySQL**
La commande utiliser pour ce conteneur était la suivante:
```PowerShell
docker run -d `
  --network todo-app --network-alias mysql `
  -v todo-mysql-data:/var/lib/mysql `
  -e MYSQL_ROOT_PASSWORD=secret `
  -e MYSQL_DATABASE=todos `
  mysql:5.7
```

1. Défnir le nouveau service et le nommer ```mysql```. Et spécifier l'image à utiliser.
```yml
version: "3.7"

services:
    app:
        # The app service definition
    mysql:
        image: mysql:5.7
```

2. Définir le volume dans ```volumes:``` puis le spécifier dans la configuration du service.

Lorsque qu'on exécutait le conteneur avec ```docker run```, le volume nommé à été créé automatiquement. Mais cela ne se produit pas lors que l'exécution avec Compose.

```yml
version: "3.7"

services:
    app:
        # The app service definition
     mysql:
        image: mysql:5.7
        volumes:
        - todo-mysql-data:/var/lib/mysql

volumes:
    todo-mysql-data:
```

3. Spécifier les variables d'environnement.
```yml
version: "3.7"

services:
    app:
        # The app service definition
     mysql:
        image: mysql:5.7
        volumes:
        - todo-mysql-data:/var/lib/mysql
        environment:
            MYSQL_ROOT_PASSWORD: secret
            MYSQL_DATABASE: todos

volumes:
    todo-mysql-data:
```

### **Exécuter la pile d'applications**

1. S'assurer qu'aucune autre copie de l'applciation/base de données ne s'exécute en premier (```docker ps``` et ```docker rm -f <ids>```).
2. Démarrer à l'aide de la commande ```docker-compose up```, ajouter le flag ```-d``` pour tout exécuter en arrière plan.
```
docker-compose up -d
```
A la suite de l'exécution de la commande, on obtient une sortie comme celle-ci:
```
Creating network "app_default" with the default driver
Creating volume "app_todo-mysql-data" with default driver
Creating app_mysql_1 ... done
Creating app_app_1   ... done
```
On peut remarquer que le volume a été créé ainsi qu'un réseau. Par défaut, Docker Compose crée automatiquement un réseau spécifiquement pour la pile d'applications.

3. Regarder les logs à l'aide de la commande ```docker-compose logs -f```. On peux observer les logs de chacun des services entrelacés dans un seul flux. 
```
mysql_1  | 2021-08-26 08:03:35+00:00 [Note] [Entrypoint]: Entrypoint script for MySQL Server 5.7.35-1debian10 started.
mysql_1  | 2021-08-26 08:03:35+00:00 [Note] [Entrypoint]: Switching to dedicated user 'mysql'
```
Le nom du service est affiché au début de la ligne (souvent en couleur) pour aider à distinguer les messages. Pour afficher les logs d'un service spécifique, ajouter le nom du service à la fin de la commande (```docker-compose logs -f app```).

### **Voir la pile d'application dans le dashboard Docker**

Dans le dashboard Docker, on peut voir un groupe nommé app. Il s'agit du nom du projet de Docker Compose qui est utilisé pour regrouper les conteneurs. Par défaut, le nom du projet est simplement le nom du répertoire dans ```docker-compose.yml```

![Stack app](/img/capture-app-docker-compose.PNG)

On y voit bien deux conteneurs que l'on a définit dans le fichier.
Les noms sont un peu plus descriptifs car ils suivent le modèle ```<project-name>_<service_name>_<replic-number>```. 

### **Détruire**

Lancer ```docker-compose down``` ou appuyez simplement sur la corbeille sur le tableau de bord Docker pour l'ensemble de l'application. Les conteneurs s'arrêteront et le réseau sera supprimé.

> **Suppression de volume**
> 
> Par défaut, les volumes nommés dans le fichier ```docker-compose.yml``` ne sont pas supprimer lors de l'exécution ```docker-compose down```. Pour les supprimer, ajouter le flag ```--volumes```.
>
> Le dashboard Docker ne supprime pas les volumes lorsque su'on supprime la pile d'application.

## **Partie 9: Meilleures pratiques en matière de création d'images**

### **Analyse de sécurité**

Lors de la création d'une image, il est recommandé de l'analyser à la recherche de failles de sécurirté à l'aide de la commande ```docker scan```. 

Docker s'est associé a [Snyk](http://snyk.io/) pour fournir le service d'analyse des vulnérabilités.

```
docker scan getting-started
```
Exemple de sortie:
```
✗ Low severity vulnerability found in openssl/libcrypto1.1
  Description: CVE-2021-3712
  Info: https://snyk.io/vuln/SNYK-ALPINE311-OPENSSL-1569447
  Introduced through: openssl/libcrypto1.1@1.1.1k-r0, openssl/libssl1.1@1.1.1k-r0, apk-tools/apk-tools@2.10.6-r0, libtls-standalone/libtls-standalone@2.9.1-r0, python2/python2@2.7.18-r0
  From: openssl/libcrypto1.1@1.1.1k-r0
  From: openssl/libssl1.1@1.1.1k-r0 > openssl/libcrypto1.1@1.1.1k-r0
  From: apk-tools/apk-tools@2.10.6-r0 > openssl/libcrypto1.1@1.1.1k-r0
  and 6 more...
  Image layer: Introduced by your base image (node:12.22.5-alpine3.11)
  Fixed in: 1.1.1l-r0
```

Lire la [documentation sur le scan de Docker](https://docs.docker.com/engine/scan/).

### **Les couches d'images**

En utilisant la commande ```docker image history```, on peut voir la commande qui a été utilisée pour créer chaque calque dans une image.

```
docker image history getting-started
```
Pour obtenir la sortie complète:
```
docker image history --no-trunc getting-started
```

### **Mise en caches des couches**

Il faut restructurer le fichier Dockerfile pour aider à prendre en charge la mise en cache des dépendances.

1. Mettre à jour le Dockerfile pour copier dans le package.json, installer les dépendances, puis copier tout le reste.
```dockerfile
# syntax=docker/dockerfile:1
 FROM node:12-alpine
 WORKDIR /app
 COPY package.json yarn.lock ./
 RUN yarn install --production
 COPY . .
 CMD ["node", "src/index.js"]
```
2. Créer un fichier nommé ```.dockerignore``` au même niveau que le fichier Dockerfile. Ajouter le contenu suivant:
```
node_modules
```
3. Contruire une nouvelle image.
```
docker build -t getting-started .
```
Les couches ont été reconstruites.

4. Modifier le fichier ```src/static/index.html```

5. Construire l'image.
La construction a été beaucoup plus rapide car les étapes 1 à 4 sont toutes ```using cache```.



